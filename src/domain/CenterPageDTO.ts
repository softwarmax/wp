export class CenterPageDTO {
    public _centerName: string;
    public _centerSearchChip: any;
    public _centerSearchWrapper: any;
    public _titleSearch : string;
    public _titleForm : string;
    public _centerSearchLoad : string;
    public _centerSearchLoadList : Array<any>;
    public _valueText : string;

    constructor() {
        this._centerName = null;
        this._centerSearchChip = null;
        this._centerSearchWrapper = null;
        this._titleSearch = null;
        this._titleForm  = null;
        this._centerSearchLoad = null;
        this._centerSearchLoadList = null;
        this._valueText = null;
    }
}