import * as React from "react";
import TextField from 'material-ui/TextField';
import TextChips from "../text/TextChips";
import InputText from "../input/InputText";
import ButtonSearch from "../input/ButtonSearch";
import ButtonClear from "../input/ButtonClear";

export interface ICenterSearch {
    onChangeText?: string;
    onSearchCenter?: string;
    _centerSearchLoad?: string;
    onClear?: string;
    _valueText?: string;
}

export interface IState {

}

export default class CenterSearch extends React.Component<ICenterSearch,{}> {
    public constructor(props) {
        super(props);
        /*<ButtonSearch onChangeText={this.props.onChangeText}
                                      _centerSearchLoad ={this.props._centerSearchLoad}
                                      onSearchCenter={this.props.onSearchCenter}/> */
    }

    public render() {
        return (
            <div>
                    <div className="row">
                                
                        <InputText _valueText={this.props._valueText}
                                   onChangeText={this.props.onChangeText}/>
                     
                    </div>

                    <div className="row">

                             
                        
                         

                    </div>

                    <div className="row">

                          
                        <ButtonClear onClear={this.props.onClear}/>

                    </div>
            </div>
        );
    }
}