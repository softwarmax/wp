import * as React from "react";
import TextField from 'material-ui/TextField';

export interface IInputText {
    onChangeText?: string;
    _valueText?: string;
}

export interface IState {

}

export default class InputText extends React.Component<IInputText,{}> {
    public constructor(props) {
        super(props);
    }

    public render() {
        return (
            <div>
                    <TextField
                        id="text-field-controlled"
                        type="InputText"
                        hintText={this.props._valueText}
                        value = {this.props._valueText}
                        floatingLabelFixed = "true"
                        floatingLabelText="Floating Label Text"
                        onChange={this.props.onChangeText}/>
            </div>
        );
    }
}