import * as React from "react";
import TextField from 'material-ui/TextField';

export interface IInpuTextInvisiliby {
    onChangeText?: string;
    _hintText?: string;
}

export interface IState {

}

export default class InpuTextInvisiliby extends React.Component<IInpuTextInvisiliby,{}> {
    public constructor(props) {
        super(props);
    }

    public render() {
        return (
            <div>
                    <TextField
                        id="text-field-controlled"
                        type="text"
                        hintText={this.props._hintText}
                        floatingLabelText="Floating Label Text Invisibility"
                        onChange={this.props.onChangeText}/>
            </div>
        );
    }
}