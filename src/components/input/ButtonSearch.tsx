import * as React from "react";
import RaisedButton from 'material-ui/RaisedButton';
//import InpuTextInvisiliby from "../input/InpuTextInvisiliby";

export interface IOnSearchCenter {
    onSearchCenter?: string;
    _centerSearchLoad?: string;
    onChangeText?: string;
}

export interface IState {

}

export default class ButtonSearch extends React.Component<IOnSearchCenter,{}> {

    style;
    
    public constructor(props) {
        super(props);

        this.style = {
        margin: 12,
       };
    }


    public render() {
        return (
            <div>
    <RaisedButton label="Search" value={this.props._centerSearchLoad} primary={true} style={this.style} onClick={this.props.onSearchCenter}/>
  </div>
        );
    }
}