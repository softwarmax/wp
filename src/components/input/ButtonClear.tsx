import * as React from "react";
import RaisedButton from 'material-ui/RaisedButton';

export interface IButtonClearProps {
    onClear?: string;
}

export interface IState {

}

export default class ButtonClear extends React.Component<IButtonClearProps,{}> {

    style;
    
    public constructor(props) {
        super(props);

        this.style = {
        margin: 12,
       };
    }


    public render() {
        return (
            <div>
    <RaisedButton label="Clear" secondary={true} style={this.style} onClick={this.props.onClear}/>
  </div>
        );
    }
}