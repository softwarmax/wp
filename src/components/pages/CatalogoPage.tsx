import * as React from "react";
import {CenterPageDTO} from "../../domain/CenterPageDTO";
import CenterSearch from "../centerPageSearch/CenterSearch";
import TextChips from "../text/TextChips";
import TableList from "../table/TableList";
import AppBarHeader from "../nav/AppBarHeader";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import {FormattedDate, FormattedMessage, FormattedRelative} from "react-intl";
import {store} from "../AppPipeline";
import {updateIntl} from "react-intl-redux";
import { push } from 'react-router-redux';


export interface ICatalogoCenterPageProps {
    catalogoPage: CenterPageDTO;
    initializeCenterPage: () => any;
    textChanged: (fieldName, value) => any;
    searchCenter: (value) => any;
    clearCenter: () => any;
}

export interface IState {

}

export class CatalogoPage extends React.Component<ICatalogoCenterPageProps, IState> {

    value : string;
    public constructor(props: ICatalogoCenterPageProps) {
        super(props);
        
    }

    public componentWillMount() {
        this.props.initializeCenterPage();
      //  store.dispatch(store.getState);
    }


    private changeLanguageButtonEn(event) {
        console.log("LANGUAGE CHANGED!");

        let currentMessagesAr = {
            pakito: "Welcome Pakito!"
        }

        store.dispatch(updateIntl({
            locale: "en", messages: currentMessagesAr
        }));
    }

    private changeLanguageButtonEs(event) {
        console.log("LANGUAGE CHANGED!");

        let currentMessagesEs = {
            pakito: "Bienvenido Pakito!"
        }


        store.dispatch(updateIntl({
            locale: "es", messages: currentMessagesEs
        }));
    }

    private loadCentertable(event) {
      let fieldName = event.target.name;
      let value = event.target.value;

      this.props.textChanged(fieldName, this.value);
      this.searchCentertable(value);
    }

    private searchCentertable(value) {

    console.log("Esto es lo que se guardo : " + this.value);
       this.props.searchCenter(value);
    }

    private onClear() {

    console.log("Limpiando" );
      // this.props.clearCenter();
       store.dispatch(push("/ "))
    }
    
    


    public render() {
        return (
                <div className="container-fluid">

                            <div className="row">

                            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                                <AppBarHeader/>
                            </MuiThemeProvider>

                            </div>

                            <div className="row">

                            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                                <TextChips _centerSearchChip = {this.props.catalogoPage._centerSearchChip}
                                           _centerSearchWrapper = {this.props.catalogoPage._centerSearchWrapper}
                                           _titleSearch = {this.props.catalogoPage._titleSearch}/>
                            </MuiThemeProvider>

                            </div>

                            <div className="row">

                            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                                <CenterSearch _valueText = {this.props.catalogoPage._valueText}
                                              _centerSearchLoad = {this.props.catalogoPage._centerSearchLoad}
                                              onChangeText={this.loadCentertable.bind(this)}
                                              onClear={this.onClear.bind(this)}
                                              onSearchCenter={this.searchCentertable.bind(this)}/>
                            </MuiThemeProvider>

                            </div>

                            <div className="row">

                            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                                <TableList _centerSearchLoadList = {this.props.catalogoPage._centerSearchLoadList}/>
                            </MuiThemeProvider>

                            </div>

                       

                        

                

                </div>
        );
    }
}