import {connect} from "react-redux";

import {CatalogoPage} from "./CatalogoPage";
import * as React from "react";

//Actions
import {initializeCenterPage} from "../../actions/initializeCenterPage";
import {textChanged} from "../../actions/textChanged";
import {searchCenter} from "../../actions/searchCenter";
import {clearCenter} from "../../actions/clearCenter";

import {IReducers} from "../../reducers/IndexReducers";


const mapStateToProps = (state: IReducers) => ({
    catalogoPage: state['reducers'].CatalogoPageReducer._centerPage
});

const mapDispatchToProps = (dispatch) => ({
    initializeCenterPage: () => dispatch(initializeCenterPage()),
    textChanged: (fieldName, value) => dispatch(textChanged(fieldName, value)),
    searchCenter: (value) => dispatch(searchCenter(value)),
    clearCenter: (value) => dispatch(clearCenter())
});

export const CatalogoPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CatalogoPage);
