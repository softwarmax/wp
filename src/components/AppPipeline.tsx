import * as React from "react";
import {createStore, applyMiddleware, combineReducers} from "redux";
import * as ReduxThunk from "redux-thunk";
import {Provider} from "react-redux";

import createHistory from 'history/createBrowserHistory';
import { Route } from 'react-router';

import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';

import reducers from "../reducers/IndexReducers";

import {IntlProvider, intlReducer} from "react-intl-redux";
import {addLocaleData} from "react-intl";

import * as spanish from "react-intl/locale-data/es";
import * as english from "react-intl/locale-data/en";

import {CenterPageContainer} from "../components/pages/CenterPageContainer";
import {CatalogoPageContainer} from "../components/pages/CatalogoPageContainer";

import * as actions from '../actions/ActionConstants'
import {searchCenter} from "../actions/searchCenter";

// Adding Middleware For React-Redux
const alertDismissal = store => next => action => {
 // store.dispatch(searchCenter(""))
 if(action.type === 'INITIALIZE_CENTER_PAGE') {
    alert(`Increment button was clicked, current state is ${store.getState()} \nI will now add to it`);
  
  return next(action)
 }
}

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

addLocaleData([...spanish, ...english]);

const reducer = combineReducers({
    ...reducers,
    router: routerReducer,
    intl: intlReducer
});

export const store = createStore(
  combineReducers({
    reducers,
   routerReducer
  }),
  applyMiddleware(middleware,alertDismissal)
)

// Now you can dispatch navigation actions from anywhere!
 store.dispatch(push("/ ")) // ¡¡¡ Truco ¡¡¡ No descomentar para que funcione el recarga automatica al compilar
 //store.dispatch(push(history))



export class AppPipeline extends React.Component<{}, {}> {
    public render(): JSX.Element {
        return (
          <Provider store={store}>
    { /* ConnectedRouter will use the store from Provider automatically */ }
    <ConnectedRouter history={history}>
      <div>

          <Route path="/ " component={CenterPageContainer} /> 
          
          <Route path="/  " component={CatalogoPageContainer}/>
          
      </div>
    </ConnectedRouter>
          </Provider>
        );
    }
}
