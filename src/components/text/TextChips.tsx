import * as React from "react";
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import FontIcon from 'material-ui/FontIcon';
import SvgIconFace from 'material-ui/svg-icons/action/face';
import {blue300, indigo900} from 'material-ui/styles/colors';

export interface TextChipsProps {
    _centerSearchChip: any;
    _centerSearchWrapper: any;
    _titleSearch: string;
}

export interface IState {

}

export default class TextChips extends React.Component<TextChipsProps,{}> {

    styles;
    
    public constructor(props: TextChipsProps) {
        super(props);

        this.styles = {
        chip: {
         margin: 4,
        },
        wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
  },
};
    }

    

public handleRequestDelete() {
  alert('You clicked the delete button.');
}

public  handleTouchTap() {
  alert('You clicked the Chip.');
}

    public render() {
        return (
            <div style={this.props._centerSearchWrapper}>

        <Chip
          onTouchTap={this.handleTouchTap}
          style={this.props._centerSearchChip}
        >
          <Avatar src="https://static.websguru.com.ar/var/m_b/b0/b05/36157/559884-Imagenes-Medicas-Arrecifes-SA-logo.png" />
          {this.props._titleSearch}
        </Chip>

      </div>
        );
    }
}