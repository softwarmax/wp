import * as React from "react";
import AppBar from 'material-ui/AppBar';

export interface IButtonProps {
    type?: string;
}

export interface IState {

}

export default class AppBarHeader extends React.Component<{},{}> {

    style;
    FloatingActionButtonExampleSimple;
    
    public constructor(props) {
        super(props);
        this.style = {
        marginRight: 20,
    };
    
    this.FloatingActionButtonExampleSimple = () => (
  <div>
    <AppBar
    title="Appenco"
    iconClassNameRight="muidocs-icon-navigation-expand-more"
  />
  </div>
);



    }

    public render() {
        return (
           
        <div>

        {this.FloatingActionButtonExampleSimple()}

        </div>
        );
    }
}