import * as React from "react";
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

export interface ITableList {
    type?: string;
    _centerSearchLoadList?: Array<any>;
}

export interface IState {

}

export default class TableList extends React.Component<ITableList,{}> {

    style;
    
    public constructor(props) {
        super(props);
        this.style = {
        marginRight: 20,
    };

    }

  public buildTableColumn() {
        let columnList = [];

        if (this.props._centerSearchLoadList != null) {
            this.props._centerSearchLoadList.map((column) => {
               
                    columnList.push(<TableRow>
                    <TableRowColumn>{column._id}</TableRowColumn>
                    <TableRowColumn>{column._centerName}</TableRowColumn>
                    <TableRowColumn>{column._centerLocation}</TableRowColumn>
                   </TableRow>
                );
               
            });
        }

        return columnList;
    }

public handleRequestDelete() {
  alert('You clicked the delete button.');
}

public  handleTouchTap() {
  alert('You clicked the Chip.');
}

    public render() {
        return (
           
        <div>

        <Table>
          <TableHeader>
            <TableRow>
             <TableHeaderColumn>ID</TableHeaderColumn>
             <TableHeaderColumn>Name</TableHeaderColumn>
             <TableHeaderColumn>Province</TableHeaderColumn>
           </TableRow>
         </TableHeader>
           <TableBody>
             {this.buildTableColumn()}
           </TableBody>
         </Table>

        </div>
        );
    }
}