import {ActionConstants} from "./ActionConstants";

export interface IClearCenter {
    type: string;
}

export function clearCenter(): IClearCenter {
    return {
        type: ActionConstants.CLEAR_CENTER
    };
}