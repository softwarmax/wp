import {ActionConstants} from "./ActionConstants";

export interface ItextChanged {
    type: string;
    fieldName: string;
    value: string;
}

export function textChanged(fieldName, value): ItextChanged {
    return {
        type: ActionConstants.TEXT_CHANGED,
        fieldName: fieldName,
        value: value
    };
}