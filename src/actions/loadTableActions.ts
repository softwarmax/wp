import {ActionConstants} from "./ActionConstants";

export interface ILoadSearchCenter {
    type: string;
    rows: object;
}

export function loadTableActions(rows): ILoadSearchCenter {
    return {
        type: ActionConstants.LOAD_TABLE_ACTIONS,
        rows: rows
    };
}