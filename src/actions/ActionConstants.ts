export class ActionConstants {
    public static INITIALIZE_CENTER_PAGE: string = "INITIALIZE_CENTER_PAGE";
    public static TEXT_CHANGED: string = "TEXT_CHANGED";
    public static SEARCH_CENTER: string = "SEARCH_CENTER";
    public static LOAD_TABLE_ACTIONS: string = "LOAD_TABLE_ACTIONS";  
    public static CLEAR_CENTER: string = "CLEAR_CENTER";  
}
