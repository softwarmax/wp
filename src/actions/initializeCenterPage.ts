import {ActionConstants} from "./ActionConstants";

export interface IInitializeCenterPage {
    type: string;
}

export function initializeCenterPage(): IInitializeCenterPage {
    return {
        type: ActionConstants.INITIALIZE_CENTER_PAGE
    };
}