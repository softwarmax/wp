import {ActionConstants} from "./ActionConstants";

export interface ISearchCenter {
    type: string;
    value: string;
}

export function searchCenter(value): ISearchCenter {
    return {
        type: ActionConstants.SEARCH_CENTER,
        value: value
    };
}