import * as Q from "q";
import {AccesDataConstants} from "../constants/AccesDataConstants"

export class DataInformationDAO {

    public _id: string;
    public _centerName: string;
    public _centerLocation: string;

    constructor() {
        this._id = null;
        this._centerName = null;
        this._centerLocation = null;
    }
    
    public geaderTitle() {
        let _title = AccesDataConstants.HEADER_TITLE;
       
        return _title;
    }

}
