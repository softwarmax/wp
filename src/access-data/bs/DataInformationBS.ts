import * as Q from "q";
import {DataInformationDAO} from "../dao/DataInformationDAO";
import {store} from "../../components/AppPipeline";
import {loadTableActions} from "../../actions/loadTableActions";

export class DataInformationBS {

    public constructor() {
        
    }

    
    public loadHeaderTitle() {
        let dataInformationDAO = new DataInformationDAO();
        let _title = dataInformationDAO.geaderTitle();

        return _title;
    }

    loadTableResultHeaderColumns(valueLoadCenter) {
        this.loadTableResultHeaderColumnsPromise(valueLoadCenter)
                    .then((legendList) => {
                        store.dispatch(loadTableActions(
                            legendList
                        ));
                    })
                 }
    


    public loadTableResultHeaderColumnsPromise(valueLoadCenter) {
        let loadCenterTableList = new Array();
        let centerTableList = null;
        let primeraLetra = valueLoadCenter.charAt(0);

        let deferred;

        try {

            deferred = Q.defer();

            if(primeraLetra === "C" || primeraLetra === "c"){
                centerTableList = new DataInformationDAO();
                centerTableList._id = 1;
                centerTableList._centerName= "Carlos Aya";
                centerTableList._centerLocation = "Malaga";
                loadCenterTableList.push(centerTableList);

                centerTableList = new DataInformationDAO();
                centerTableList._id = 2;
                centerTableList._centerName= "Cardenas";
                centerTableList._centerLocation = "Almeria";
                loadCenterTableList.push(centerTableList);

                centerTableList = new DataInformationDAO();
                centerTableList._id = 3;
                centerTableList._centerName= "Caracola";
                centerTableList._centerLocation = "Caracol";
                loadCenterTableList.push(centerTableList);

            }else if(primeraLetra === "L" || primeraLetra === "l"){

                centerTableList = new DataInformationDAO();
                centerTableList._id = 1;
                centerTableList._centerName= "La Paz";
                centerTableList._centerLocation = "Madrid";
                loadCenterTableList.push(centerTableList);
                
            }else if(primeraLetra === "I" || primeraLetra === "i"){
                centerTableList = new DataInformationDAO();
                centerTableList._id = 1;
                centerTableList._centerName= "Imaginario";
                centerTableList._centerLocation = "La Luna";
                loadCenterTableList.push(centerTableList)
            }else{
                centerTableList = new DataInformationDAO();
                centerTableList._id = 1;
                centerTableList._centerName= "Centro Por Defecto";
                centerTableList._centerLocation = "centroDefecto";
                loadCenterTableList.push(centerTableList)
            }
        
        
        deferred.resolve(loadCenterTableList);

            return deferred.promise;

        } catch(Exception) {
            throw Exception;
        }
    }
    
}