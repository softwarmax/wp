import {combineReducers} from "redux";
import {CenterPageReducer, CenterPageState} from "./CenterPageReducer";
import {CatalogoPageReducer, CatalogoPageState} from "./CatalogoPageReducer";

export interface IReducers {
    CenterPageReducer: CenterPageState;
    CatalogoPageReducer: CatalogoPageState;
}

export default combineReducers<IReducers>({
    CenterPageReducer,
    CatalogoPageReducer
});

