import * as objectAssign from "object-assign";
import {Action} from 'redux';
import {CenterPageDTO} from "../domain/CenterPageDTO";
import {DataInformationBS} from "../access-data/bs/DataInformationBS";
import {DataInformationDAO} from "../access-data/dao/DataInformationDAO";
import {ActionConstants} from "../actions/ActionConstants";

export class CatalogoPageState {
    public _centerPage: CenterPageDTO;

    public constructor() {
        this._centerPage = new CenterPageDTO();
    }
}

export function CatalogoPageReducer(state: CatalogoPageState = new CatalogoPageState(), action: Action): CatalogoPageState {
    let newState: CatalogoPageState;
    let initialCenterPage: CenterPageDTO;
    let dataInformationBS;
    initialCenterPage = new CenterPageDTO();
    dataInformationBS = new DataInformationBS();

    const styles = {
            chip: {
            margin: 4,
            },
            wrapper: {
            display: 'flex',
            flexWrap: 'wrap',
            },
           };
           
            initialCenterPage._titleSearch = "Hello redux de mi vida";
            
            initialCenterPage._titleForm = "Form Add Users";
            initialCenterPage._centerName = "Facility Research One";
            initialCenterPage._centerSearchWrapper = styles.wrapper;
            initialCenterPage._centerSearchChip = styles.chip;

    switch(action.type) {
        case ActionConstants.INITIALIZE_CENTER_PAGE:

            let loadCenterTableList = new Array();
            let centerTableList = null;


            centerTableList = new DataInformationDAO();
            centerTableList._id = 1;
            centerTableList._centerName= "Centro Por Defecto";
            centerTableList._centerLocation = "centroDefecto";
            loadCenterTableList.push(centerTableList);
            initialCenterPage._valueText  = "";

            initialCenterPage._centerSearchLoadList = loadCenterTableList;

            newState = objectAssign({}, state, {_centerPage: initialCenterPage});

            return newState;

            case ActionConstants.TEXT_CHANGED:

            
            let fieldName = action['fieldName'];
            let value = action['value'];

            initialCenterPage._centerSearchLoad= value;

             newState = objectAssign({}, state, {_centerPage: initialCenterPage});

             return newState;

            case ActionConstants.SEARCH_CENTER:

             let valueLoadCenter = action['value']

             initialCenterPage._centerSearchLoad = valueLoadCenter;

             dataInformationBS.loadTableResultHeaderColumns(valueLoadCenter);

             newState = objectAssign({}, state, {_centerPage: initialCenterPage});

             return newState;

             case ActionConstants.LOAD_TABLE_ACTIONS:

             let valueLoadlistCenter = action['rows']

             initialCenterPage._centerSearchLoadList = valueLoadlistCenter;

             newState = objectAssign({}, state, {_centerPage: initialCenterPage});

             return newState;

             case ActionConstants.CLEAR_CENTER:
             
             initialCenterPage._centerSearchLoad = "";
             initialCenterPage._valueText  = "";

             newState = objectAssign({}, state, {_centerPage: initialCenterPage});

             return newState;

        default:
            return state;

    }
}