let path = require('path');
let webpack = require("webpack");
let HtmlWebpackPlugin = require('html-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let CleanWebpackPlugin = require('clean-webpack-plugin');

let basePath = __dirname;

console.log("Ruta Base: " + basePath);

module.exports = function(env) {

    return {
        context: path.join(basePath, 'src'),
        resolve: {
            // .js is required for react imports.
            // .tsx is for our app entry point.
            // .ts is optional, in case you will be importing any regular ts files.
            extensions: ['.js', '.ts', '.tsx']
        },
        target: "web",
        node: {
            fs: "empty",
            net: "empty"
        },
        entry: [
            '../node_modules/bootstrap/dist/js/bootstrap.min.js',
            '../node_modules/bootstrap/dist/css/bootstrap.css',
            '../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css',
            './scss/index.scss',
            '../index.tsx'
        ],

        output: {
            path: path.join(basePath, './dist'),
            filename: '[hash].[name].js'
            //filename: 'bundle.js'
        },

        // http://webpack.github.io/docs/configuration.html#devtool
        devtool: 'inline-source-map',

        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'awesome-typescript-loader'
                    }]
                },
                {
                    test: /\.(html)$/,
                    exclude: /node_modules/,
                    loader: 'html-loader'
                },
                {
                    test: /\.(js)$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                },
                {
                    test: /\.(csv|tsv)$/,
                    exclude: /node_modules/,
                    loader: 'dsv-loader',
                },
                {
                    test: /\.json$/,
                    exclude: /node_modules/,
                    loader: 'json-loader',
                },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    loader: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            { loader: 'css-loader', },
                            { loader: 'sass-loader', },
                        ],
                    }),
                },
                {
                    test: /\.css$/,
                    include: /node_modules/,
                    loader: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: {
                            loader: 'css-loader',
                        },
                    }),
                },
                // Loading glyphicons => https://github.com/gowravshekar/bootstrap-webpack
                // Using here url-loader and file-loader
                {
                    test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'url-loader?limit=10000&mimetype=application/font-woff'
                },
                {
                    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
                },
                {
                    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'file-loader'
                },
                {
                    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
                },
            ]
        },

        plugins:[
            new CleanWebpackPlugin(['./dist'], {
                //root: basePath,
                verbose: true,   // Write logs to console
                dry: false,     // Use boolean "true" to test/emulate delete. (will not remove files).
                                // (Default: "false", remove files)
                watch: true     // If true, remove files on recompile. (Default: false)
                //exclude: ['RunExpress.js']
            }),
            //Generate index.html in /dist => https://github.com/ampedandwired/html-webpack-plugin
            new HtmlWebpackPlugin({
                filename: 'index.html', //Name of file in ./dist/
                template: 'index.html', //Name of template in ./src
                hash: true,
            }),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            }),
            new webpack.optimize.CommonsChunkPlugin({
                names: ['vendor', 'manifest'],
            }),
            new ExtractTextPlugin({
                filename: '[hash].[name].css',
                //filename: 'styles.css',
                disable: false,
                allChunks: true,
            }),
        ]
    }
}
