class RunExpress {
    constructor() {
        this.startWebServer();
    }

    startWebServer() {
        let express = require('express');
        let expressWebServer = express();

        expressWebServer.use(express.static(__dirname + '/dist/'));

        expressWebServer.listen(3001,() => {
            console.log('Express WebServer Started port: ' + 3001);
            console.log('Reading Directory: ' + __dirname + '/dist');
        });
    }
}

new RunExpress();